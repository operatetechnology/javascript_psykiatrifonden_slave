var gulp = require('gulp'),
    concat = require('gulp-concat');
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    del = require('del'),
    uglify = require('gulp-uglify');
    order = require('gulp-order');
    watch = require('gulp-watch');

gulp.task('make', function() {
  return gulp.src('src/**/*.js')
    .pipe(order([
        "underscore.js",
        "backbone.js",
        "app.js",
    ]))
    //.pipe(uglify())
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist/'))
    .pipe(notify({ message: 'Default task complete' }));
});

gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('watch', function() {
  gulp.watch('src/**/*.js', ['make']);
});

gulp.task('dist', function() {
    return gulp.src('src/**/*.js')
    .pipe(order([
        "underscore.js",
        "backbone.js",
        "app.js",
    ]))
    .pipe(uglify())
    .pipe(concat('counselling-app-base.js'))
    .pipe(gulp.dest('dist/'))
    .pipe(notify({ message: 'Default task complete' }));
});
