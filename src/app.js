(function(Backbone, $) {

    templateCache = {};

    path = {
        'api': 'https://psykiatrifonden.omega.oitudv.dk/api/v1',
        'template': 'https://psykiatrifonden.omega.oitudv.dk/question/template'
    };

    var TopicModel = Backbone.Model.extend({
        defaults: {
            tid: '',
            field_topic: ''
        }
    });

    var TopicCollection = Backbone.Collection.extend({
        model: TopicModel,

        url: function(group) {
            return path.api + '/questions/topic/' + group + "?_format=json";
        },

        initialize: function(models, options) {
            this.url = this.url(options.group);
        }
    });

    var TopicView = Backbone.View.extend({

        selectedTid: '',

        el: 'div.topic-outer',
        fetchedOnce: false,

        events: {
            'click div.topic-inner' : function() {
                this.fetchOnce();
                this.$el.find('div.dropdown').toggleClass('active');

                if (this.$el.find('div.dropdown').hasClass('active')) {
                    this.attachWindowClickListener();
                } else {
                    this.detachWindowClickListener();
                }
            }
        },

        windowClickListener: function(event) {
            $('div.dropdown').trigger('click');
        },

        attachWindowClickListener: function() {
            event.stopPropagation();
            $(window).on('click', this.windowClickListener);
        },

        detachWindowClickListener: function() {
            $(window).off('click', this.windowClickListener);
        },

        fetchOnce: function(options) {
            if (this.fetchedOnce) {
                return;
            }
            this.collection.fetch(options);
            this.fetchedOnce = true;
        },

        initialize: function() {
            this.$el.empty();
            this.$el.append('<div class="topic-inner"><div class="dropdown"><span class="label">Emner</span><ul></ul></div></div>');

            this.listenToOnce(this.collection, 'sync', this.render);
        },

        render: function () {
            var self = this;
            var html = '';

            var classes = '';
            if (!self.selectedTid) {
                classes += ' class="active"';
            }
            html += '<li' + classes + '><a href="#">Alle</a></li>';

            this.collection.each(function(model) {

                var classes = '';
                if (self.selectedTid == model.get('tid')) {
                    classes += ' class="active"';
                }

                html += '<li' + classes + '><a href="#topic/' + model.get('tid') + '/page/1">' + model.get('field_topic') + '</a></li>';
            });

            this.$el.find(".dropdown ul").append(html);
        },

        setDefaultValue: function(value) {
            this.selectedTid = value;
        }

    });

    var SearchView = Backbone.View.extend({

        el: 'div.search-inner',

        events: {
            'submit form': function(event) {

                window.router.navigate('keyword/' + encodeURIComponent($('input[name="search"]', this.$el).val()) +'/page/1', {trigger: true});
                event.preventDefault();

            }
        },

        render: function() {
            this.$el.append('<form><input type="search" name="search" placeholder="Indtast søgeord..." /><input type="submit" value="Søg" /></form>');
        },

        setDefaultValue: function(value) {
            this.$el.find('input[name="search"]').val(value);
        }

    });

    var SlugView = Backbone.View.extend({

        el: 'div.slug-inner',

        events: {
            'submit form': function(event) {

                window.router.navigate('question/show/' + encodeURIComponent($('input[name="slug"]', this.$el).val()), {trigger: true});
                event.preventDefault();

            }
        },

        render: function() {
            this.$el.append('<form><input type="text" name="slug" placeholder="Indtast ID..." /><input type="submit" value="Slå op" /></form>');
        },

        setDefaultValue: function(value) {
            this.$el.find('input[name="slug"]').val(value);
        }

    });

    var QuestionModel = Backbone.Model.extend({

        url: function() {
            return path.api + '/question/' + this.get('nid') + "?_format=json";
        },

        defaults: {
            nid: '',
            title: null,
            field_slug: null,
            field_question_body: null,
            field_answer_body: null,
            field_region: null,
            field_age: null,
            field_gender: null,
            field_group: null,
            field_referrer: null,
            field_referrer_info: null,
            field_topic: null,
            links: null,
            topics: null
        },

        parse: function(data) {
            if (data[0]) {
                return data[0];
            } else {
                return data;
            }
        },

        sync: function(method, model, options) {
            if(method === 'create') {
                options.url = path.api + '/q?_format=json';
                options.beforeSend = function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Basic d2Vic2l0ZTprNm4yeHJrZmdaRUZkTGZwcCl2Q05LWGp2');
                };
            }
            return Backbone.sync.apply(this, arguments);
        },

        validate: function(attrs, options) {
            var errors = [];

            if (!attrs.field_question_body) {
                errors.push({name: 'field_question_body', message: 'Skriv spørgsmål'});
            }

            if (!attrs.field_gender) {
                errors.push({name: 'field_gender', message: 'Vælg køn'});
            }

            if (!attrs.field_age) {
                errors.push({name: 'field_age', message: 'Vælg alder'});
            }

            if (!attrs.field_region) {
                errors.push({name: 'field_region', message: 'Vælg region'});
            }

            if (!attrs.field_referrer) {
                errors.push({name: 'field_referrer', message: 'Vælg hvor du hørt om Psykiatrifondens Rådgivning'});
            }

            if (attrs.field_referrer === 'other' && !attrs.field_referrer_info) {
                errors.push({name: 'field_referrer_info', message: 'Skriv hvor du hørt om Psykiatrifondens Rådgivning'});
            }

            return errors.length > 0 ? errors : false;
        }
    });

    var QuestionsCollection = Backbone.Collection.extend({
        model: QuestionModel
    });

    var RequestModel = Backbone.Model.extend({

        itemsPerPage: 9,

        url: function() {
            return this.getBaseAndParams().remoteRoute;
        },

        initialize: function() {
        },

        getBaseAndParams: function() {
            if (this.get('topic')) {
                return {
                    remoteRoute: path.api + '/questions/' + this.get('group') + '/' + this.get('topic') + '?_format=json&items_per_page=' + this.itemsPerPage + '&offset=' + ((this.get('page') * this.itemsPerPage) - this.itemsPerPage),
                    localRoute: path.api + '#topic/' + this.get('topic')
                };

            } else if (this.get('keyword')) {
                return {
                    remoteRoute: path.api + '/questions/search/' + this.get('group') + '?_format=json&keys=' + this.get('keyword') + '&items_per_page=' + this.itemsPerPage + '&offset=' + ((this.get('page') * this.itemsPerPage) - this.itemsPerPage),
                    localRoute: '#keyword/' + this.get('keyword')

                };
            } else {
                return {
                    remoteRoute: path.api + '/questions/' + this.get('group') + '?_format=json&items_per_page=' + this.itemsPerPage + '&offset=' + ((this.get('page') * this.itemsPerPage) - this.itemsPerPage),
                    localRoute: '#'
                };
            }
        },

        defaults: {
            base: null,
            params: null,
            group: null,
            topic: null,
            keyword: null,
            page: null,
            meta: null,
            items: null
        }
    });

    var RequestView = Backbone.View.extend({
        el: '*[data-app-key="counselling"]',

        initialize: function() {
            this.$el.addClass('counselling-app');
            this.model.set({'group': this.$el.data('app-group')});
            this.listenTo(this.model, 'sync', this.render);
            this.model.fetch();
        },

        render: function() {
            this.$el.empty();
            this.$el.append('<div class="topic-outer"><div class="topic-inner"></div></div>');
            this.$el.append('<div class="slug-outer"><div class="slug-inner"></div></div>');
            this.$el.append('<div class="search-outer"><div class="search-inner"></div></div>');
            this.$el.append('<div class="new"><a href="#question/new">Indsend nyt spørgsmål</a></div>');
            this.$el.append('<div class="list"></div>');
            this.$el.append('<div class="pagination"></div>');

            var questions = new QuestionsCollection(this.model.get('items'));
            var questionsList = new QuestionsListView({collection: questions});

            var pagerLinks = Math.ceil((parseInt(this.model.get('meta').total) + parseInt(this.model.get('meta').offset)) / parseInt(this.model.get('meta').itemsPerPage));

            if (pagerLinks > 1) {
                for (var i = 0; i < pagerLinks; i++) {
                    var active = 'inactive';
                    if (i + 1 === this.model.get('page')) active = 'active';
                    var pagerLink = '<a href="' + this.model.getBaseAndParams().localRoute + '/page/' + (i + 1) + '" class="' + active +'">' + (i + 1) + '</a>';
                    this.$el.find('div.pagination').append(pagerLink);
                }
            }

            var topics = new TopicCollection(null, {'group': this.model.get('group')});
            var topicsListView = new TopicView({collection: topics});

            if (this.model.get('topic')) {
                topicsListView.setDefaultValue(this.model.get('topic'));
            }

            var searchView = new SearchView();
            searchView.render();

            if (this.model.get('keyword')) {
                searchView.setDefaultValue(this.model.get('keyword'));
            }

            var slugView = new SlugView();
            slugView.render();

            return this;
        }
    });

    var QuestionsListView = Backbone.View.extend({
        el: 'div.list',

        initialize: function() {
            this.render();
        },

        render: function() {
            var html;
            this.collection.each(function(model) {
                html = '<div class="question-item-outer"><a href="#question/show/' + model.get('field_slug') + '?_format=json"><div class="question-item-inner">' + '<div clasS="date">' + model.get('created') + '</div><h2>' + model.get('title') + '</h2><div class="body">' + model.get('field_question_body') + '</div></div></a></div>';
                this.$el.append(html);
            }, this);

            return this;
        }
    });

    var QuestionView = Backbone.View.extend({
        el: '*[data-app-key="counselling"]',

        initialize: function() {
            this.$el.addClass('counselling-app');

            this.listenTo(this.model, 'sync', this.render);
            this.model.fetch();
        },

        renderTemplate: function(template, model) {
            var html = template(
                {
                    'title': model.get('title'),
                    'question': model.get('field_question_body'),
                    'answer': model.get('field_answer_body'),
                    'links': model.get('links'),
                    'topics': model.get('topics')
                }
            );

            this.$el.html(html);
        },

        render: function() {
            var self = this;
            if (self.model.get('title')) {
                if(!templateCache['show']) {
                    $.ajax({
                        url: path.template + '/show',
                        method: 'GET',

                        success: function(data) {
                            var template = _.template(data);
                            templateCache['show'] = template;
                            self.renderTemplate(template, self.model);
                        }
                    });
                } else {
                    self.renderTemplate(templateCache['show'], self.model);
                }
            } else {

                if(!templateCache['questionMissing']) {
                    $.ajax({
                        url: path.template + '/questionMissing',
                        method: 'GET',

                        success: function(data) {
                            var template = _.template(data);
                            templateCache['questionMissing'] = template;
                            self.renderTemplate(template, self.model);
                        }
                    });
                } else {
                    self.renderTemplate(templateCache['questionMissing'], self.model);
                }
            }

            return this;
        }

    });

    var QuestionNewView = Backbone.View.extend({
        el: '*[data-app-key="counselling"]',

        events: {
            "submit form" : "submitForm",
            "change select#question-reference" : "referenceSelectChange"
        },

        initialize: function() {
            self = this;

            this.$el.addClass('counselling-app');

            this.model.set('field_group', this.$el.data('app-group'));

            this.model.on('invalid', function (model, errors, options) {

                self.$el.find('.validation-error').remove();
                _.each(errors, function (error) {

                    if (error.name === 'field_question_body') {
                        self.$el.find('textarea[name="question-body"]').after('<div class="validation-error">' + error.message +'</div>');
                    }

                    if (error.name === 'field_gender') {
                        self.$el.find('select[name="question-gender"]').after('<div class="validation-error">' + error.message +'</div>');
                    }

                    if (error.name === 'field_age') {
                        self.$el.find('select[name="question-age"]').after('<div class="validation-error">' + error.message +'</div>');
                    }

                    if (error.name === 'field_region') {
                        self.$el.find('select[name="question-region"]').after('<div class="validation-error">' + error.message +'</div>');
                    }

                    if (error.name === 'field_referrer') {
                        self.$el.find('select[name="question-reference"]').after('<div class="validation-error">' + error.message +'</div>');
                    }

                    if (error.name === 'field_referrer_info') {
                        self.$el.find('input[name="question-reference-other"]').after('<div class="validation-error">' + error.message +'</div>');
                    }

                });
            });
        },

        template: 'new',

        renderTemplate: function(template) {
            self = this;
            var html = $(template());
            //$('form', html).on('submit', null, self, this.submitForm);
            this.$el.html(html);
            this.delegateEvents();
        },

        render: function() {
            var self = this;

            if(!templateCache['new']) {
                $.ajax({
                    url: path.template + '/new',
                    method: 'GET',

                    success: function(data) {
                        var template = _.template(data);
                        templateCache['new'] = template;
                        self.renderTemplate(template);
                    }
                });
            } else {
                self.renderTemplate(templateCache['new']);
            }

            return this;
        },

        submitForm: function() {
            var self = this;

            this.model.set('field_question_body', $('textarea[name="question-body"]', this.$el).val());
            this.model.set('field_gender', $('select[name="question-gender"]', this.$el).val());
            this.model.set('field_age', $('select[name="question-age"]', this.$el).val());
            this.model.set('field_region', $('select[name="question-region"]', this.$el).val());
            this.model.set('field_referrer', $('select[name="question-reference"]', this.$el).val());
            this.model.set('field_referrer_info', $('input[name="question-reference-other"]', this.$el).val());

            this.model.save({type: 'POST'}, {
                success: function(model, response, options) {
                    self.$el.find('.question-form-outer .question-form-inner').html('<h2>Tak for dit spørgsmål</h2><p>Vi vil gøre vores bedste for at svare hurtigst muligt og senest indenfor 10 dage.</p><p>Dit spørgsmål har fået ID-nummer: <b>' + response + '</b></p><p>Når svaret er klar, kan du finde det på forsiden af brevkassen, eller ved at søge på dit ID-nummer på forsiden af brevkassen.</p><p>Mange hilsner fra<br />Psykiatrifondens Rådgivning</p>');
                    window.scrollTo(0, 0);

                },
                error: function(model, response, options) {

                }
            });

            event.preventDefault();
        },

        referenceSelectChange: function(event) {
            var self = this;
            if (event.target.value === 'other') {
                self.$el.find('#question-reference-other-wrapper').removeClass('hidden');
            } else {
                self.$el.find('#question-reference-other-wrapper').addClass('hidden');
            }
        }

    });

    var AppRouter = Backbone.Router.extend({

        routes: {
            '': 'questionsOffset',
            'page/:page': 'questionsOffset',
            'topic/:topic/page/:page': 'questionsOffsetWithTopic',
            'keyword/:keyword/page/:page': 'questionsOffsetWithKeyword',
            'question/show/:nid': 'question',
            'question/new' : 'newQuestion'
        },

        question: function(nid) {
            var question = new QuestionModel();
            question.set({nid: nid});
            var questionView = new QuestionView({model: question});
        },

        newQuestion: function() {
            var questionModel = new QuestionModel();
            var questionNew = new QuestionNewView({ model: questionModel });
            questionNew.render();
        },

        questionsOffset: function(page) {
            page = page || 1;
            var request = new RequestModel({
                page: parseInt(page)
            });

            var requestView = new RequestView({model: request});
        },

        questionsOffsetWithTopic: function(topic, page) {
            page = page || 1;

            var request = new RequestModel({
                page: parseInt(page),
                topic: parseInt(topic)
            });

            var requestView = new RequestView({model: request});
        },

        questionsOffsetWithKeyword: function(keyword, page) {
            page = page || 1;

            var request = new RequestModel({
                page: parseInt(page),
                keyword: keyword
            });

            var requestView = new RequestView({model: request});
        }
    });

    var addCss = function(callback) {
        var head = document.head;
        var link = document.createElement('link');
        link.addEventListener('load', function() {
            callback();
        });

        if (!document.addEventListener) {
            callback();
        }

        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = 'https://bitbucket.org/operatetechnology/javascript_psykiatrifonden_slave/downloads/app.css';

        head.appendChild(link);
    };

    $(document).ready(function() {
        if ($('*[data-app-key="counselling"]').length) {
            $('html').addClass('counselling-app-on-page');
            addCss(function() {
                window.router = new AppRouter;
                Backbone.history.start();
            });
        }
    });

})(Backbone, jQuery);
